Dieses Projekt enthält die benötigten Daten zum Erstellen des LCB-Photometers. Dies ist ein einfaches Photometer, welches LEDs als Lichtquelle und einen einfachen Lichtsensor verwendet. Die Daten werden über ein Wemos D1 mini verarbeitet und das Photometer kann über ein internetfähiges Gerät gesteuert werden. 

Das veröffentlichte Modell basiert auf dem Photometer von Dipl.-Ing. Ulrich Scheffler der HAW Hamburg (https://www.haw-hamburg.de/fakultaeten-und-departments/ls/ls-forschung/projekte/projekte-aus-der-chemie/schuman/smartphone-photometer.html). Zum Bau des eigentlichen Photometers wird auf die enthaltene Anleitung von U. Scheffler verwiesen. Bei dem LCB-Photometer wurde das Skript sowie das Gehäuse auf die eigenen Wünsche angepasst. Für die Verwendung des Photometers steht eine Kurzanleitung zur Verfügung.

Vielen Dank an Ulrich Scheffler für die Erlaubnis der Veröffentlichung und das spannende Projekt.
