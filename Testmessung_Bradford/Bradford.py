import pandas as pd
from scipy import stats
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl

def polyfit(x, y, degree):
    results = {}

    coeffs = np.polyfit(x, y, degree)

     # Polynomial Coefficients
    results['polynomial'] = coeffs.tolist()

    # r-squared
    p = np.poly1d(coeffs)
    # fit values, and mean
    yhat = p(x)                         # or [p(z) for z in x]
    ybar = np.sum(y)/len(y)          # or sum(y)/len(y)
    ssreg = np.sum((yhat-ybar)**2)   # or sum([ (yihat - ybar)**2 for yihat in yhat])
    sstot = np.sum((y - ybar)**2)    # or sum([ (yi - ybar)**2 for yi in y])
    results['determination'] = ssreg / sstot

    return results

raw_data = pd.read_csv("Messdaten.csv" , sep=";" , decimal = ",")

def plotter(x_values, y_values , filename):

    '''
    x_values = pandas series with the x values
    y_values = pandas DataFrame with two columns
    '''
    x_values = x_values.copy()
    x_values.drop(0 , inplace = True)

    x_values = pd.concat([x_values, x_values])

    y_temp_1= y_values.iloc[: , 0] - y_values.iloc[: , 0][0]
    y_temp_1.drop(0, inplace=True)
    y_temp_2= y_values.iloc[: , 1] - y_values.iloc[: , 1][0]
    y_temp_2.drop(0, inplace=True)

    y_values = pd.concat([y_temp_1, y_temp_2])

    Hoehe =  10
    Breite = 15

    pgf_with_rc_fonts = {
        'text.usetex' : True,
        'font.family' : "sans-serif",

        'font.size' : 20,
        'axes.labelsize' : 20,
        'xtick.labelsize' : 20,
        'ytick.labelsize' : 20,
        'text.latex.preamble' : [r'\usepackage{sfmath}']
        }
    mpl.rcParams.update(pgf_with_rc_fonts)

    

    slope , intercept,r_value ,p_value, std_err= stats.linregress(x_values , y_values)
    fit_fn = np.poly1d((slope , intercept))

    poly_fit = np.poly1d(np.polyfit(x_values, y_values, 3))


    fig, ax = plt.subplots(figsize=(Breite,Hoehe ))

    ax.plot(x_values , y_values,'x',color='black',markersize=10, mew=3)
    #ax.plot(x_values,fit_fn(x_values),color ="black")
    ax.plot(np.arange(0,160),poly_fit(np.arange(0,160)),color ="black")

    figtext = ""
    for x in list(range(0,len(poly_fit)+1)):
        
        temp = '{:0.2E}'.format(poly_fit[x])

        if x > 0:
            temp = temp + r"~x$^" + str(x) + r"$~"
            
        if poly_fit[x] > 0:
            temp = r"+~" + temp
        figtext = temp + figtext
    
    plt.figtext(0.15,0.8, r"y~=~" + figtext + "\n"
                + r"$R^2$~=~" + "{:0.5f}".format(polyfit(x_values, y_values , len(poly_fit))["determination"]))
    ax.set_xlabel(r'$c$ [\textmu g / mL]')
    ax.set_ylabel(r'E')

            #ax.axis([x_values[0]-(x_values[0]/2),x_values[-1]+(x_values[-1]/15),y_values[0]-(y_values[0]/2),y_values[-1]+(y_values[-1]/15)])

    #plt.savefig(filename + ".pdf")
    plt.show()

    return poly_fit

plotter(raw_data["Konzentration"] , raw_data[["E_1_Vergleich" , "E_2_Vergleich"]] ,"x")
