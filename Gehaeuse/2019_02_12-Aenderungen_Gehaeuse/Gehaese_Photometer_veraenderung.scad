// Gehäuse für Photometer mit WeMos D1 mini
// U. Scheffler 09.01.2017
$fn=50;
//Innenmaße
ds = 3.2;//1.6; // Dicke Seitenwand
bi = 35.2;//35.0; // Breite innen
li = 36.2;//36.0; // Länge innen
wi = 0.8; // Wandstärke Innen
hi = 26.0; // Höhe innen
kr = 1.6; // Kanten Radius
//Deckel
dd = 3.6;//2.0; // Dicke Deckel
df = 0.8; // Deckel Führungsnut
fl = 0.2; // Freilauf für Deckelführung
//Rastnasen
tr = 1.2; // Tiefe der Rastnase
lr = 8.0; // Länge der Rastnase
//USB Anschluss
bu = 8.0; // Breite USB
hu = 11.0; // Höhe USB
tu = 0.4; // Zusätzliche Tiefe USB
//Küvette
kk = 13.2; // Kantenlänge für Küvettenaussparung
//Sensor
bs = 5.0; // Einbaubreite Lichtsensor
dc = 3.0; // Duchmesser Sensor Chip
// Führung für Platinen
dw = 5.0; // Dicke für WeMos D1 mini
bf = 10.0; // Breite Führungsblock
lf = 8.0; // Länge Führungsblock
duw= 3.0; // Dicke unter WiMos D1 mini
afu= 30.0; // Abstand der Führung unter der Platine zur USB Seite
// LED Führung
bl = 5.5; // Breite des Ausschitts für die LED
ll = 7.5; // Länge der Auflagefläche für die LED
// Beschriftungen
tb = 0.4; // Einprägetiefe
bpf= 4.0; // Breite Pfeilschaft
lpf= 10.0; // Länge Pfeilschaft
dpf= 0.5; // Breite der Prägelinie
apf= 1.0; // Abstand Pfeil zum Küvettendurchbruch
module roundcube(x,y,z,r){ // "cube" mit abgerundeten Kanten und Radius = r
hull(){
translate([ r, r, r]) sphere(r=r);
translate([x-r, r, r]) sphere(r=r);
translate([ r,y-r, r]) sphere(r=r);
translate([x-r,y-r, r]) sphere(r=r);
translate([ r, r,z-r]) sphere(r=r);
translate([x-r, r,z-r]) sphere(r=r);
translate([ r,y-r,z-r]) sphere(r=r);
translate([x-r,y-r,z-r]) sphere(r=r);}
}

module gehaeuse(){  // Gehäuse
    difference(){
        translate([ 0, 0, 0]) roundcube(ds+bi+ds,ds+li+ds,ds+hi+dd,kr); // Außenhülle
        translate([ds,ds,ds]) cube([bi,li,hi+dd]);			// Innenraum
	translate([ds+bi+ds-bu,0,ds+hi/2-hu/2]) cube([bu,ds+tu,hu]);	// USB Anschluss
	translate([ds,0,ds+hi]) cube([bi,ds+li+ds,dd ]);		// Deckelloch
	translate([ds-df,0,ds+hi]) cube([df+bi+df,ds+li+ds,dd/2]);	// Deckelführung
	translate([ds,ds+bs+wi,0]) cube([kk,kk,ds]);			// Küvettenführung Bodendurchbruch
	//translate([ds+kk/2-bpf,ds+bs+wi+kk+wi+apf,0]) linear_extrude(height = tb){difference(){ // Beschriftung Pfeil
	//	polygon(points=[[bpf,0],[0,bpf],[1/2*bpf,bpf],[bpf/2,bpf+lpf],[3*bpf/2,bpf+lpf],[3*bpf/2,bpf],[2*bpf,bpf]]);
	//	polygon(points=[[bpf,sqrt(2)*dpf],[sqrt(2)*dpf+dpf,bpf-dpf],[1/2*bpf+dpf,bpf-dpf],[bpf/2+dpf,bpf+lpf-dpf],
	//			[3*bpf/2-dpf,bpf+lpf-dpf],[3*bpf/2-dpf,bpf-dpf],[2*bpf-sqrt(2)*dpf-dpf,bpf-dpf]]);}}
//	translate([ds+bi/2,   ds+li+ds-tb,30]) rotate([90,180,180]){ // Beschriftung Text
	//	linear_extrude(height = tb){
	//		translate ([0,25,0]) text("WIFI",
	//			font = "Liberation Sans:style=Bold", size = 4.5, valign = "center", halign = "center");
	//		translate ([0,19,0]) text("Photometer",
	//			font = "Liberation Sans:style=Bold", size = 4.5, valign = "center", halign = "center");
	//		translate ([0,12,0]) text("192.168.4.1" ,
	//			font = "Liberation Sans:style=Bold", size = 4.5, valign = "center", halign = "center");
	//		translate ([0,6,0]) text("JLU",
	//			font = "Liberation Sans:style=Bold", size = 3.0, valign = "center", halign = "center");}}
                }

	difference(){							// Führung für Platinen
		translate([ds+bi-dw-bf, ds, ds]) cube([bf,lf,hi]);	// Außen
		translate([ds+bi-dw-bf+wi, ds+2*wi, ds]) cube([bf-2*wi,lf-1*wi,hi]); // Innen
		translate([ds+bi+ds-bu,0,ds+hi/2-hu/2]) cube([bu,ds+tu,hu]);// USB
	}

	difference(){								// Küvettenführung und LED Halter
		union(){
			translate([ds-wi,ds+bs,ds]) cube([kk+2*wi,kk+2*wi,hi]);	// Küvettenführung
			translate([ds-wi+kk/2-bl/2,ds+bs+wi+kk,ds]) cube([wi+bl+wi,ll,hi]); // LED Halter
		}
		translate([ds,ds+bs+wi,ds]) cube([kk,kk,hi]);			// Küvettenführung Bodendurchbruch
		translate([ds-wi+kk/2-bl/2+wi,ds+bs+wi+kk,ds+hi/2]) cube([bl,ll,hi/2]); // LED Halter Aussparung
		translate([ds-wi+kk/2+wi,ds+bs+wi+kk,ds+hi/2]) rotate ([-90,0,0]) cylinder(h=ll, d=bl);// LED Aussparung
		hull(){
			translate([ds-wi+kk/2+wi,ds+bs,ds+hi/2+dc/2]) rotate ([-90,0,0]) cylinder(h=wi, d=dc);	// Sensor Lichtweg
			translate([ds-wi+kk/2+wi,ds+bs,ds+hi/2-dc/2]) rotate ([-90,0,0]) cylinder(h=wi, d=dc);	// Sensor Lichtweg
		}
	}

	translate([ds+bi-duw,ds+afu,ds]) cube([duw,wi,hi]);// Platinenführung
	//translate([ds+bi/2-lr/2+tr/2,ds+tr/2,ds+hi]) sphere(, d=tr);// Rastnase 1 Rundung oben
	//translate([ds+bi/2-lr/2,ds,ds+hi-tr]) cube([tr,tr,tr]); // Rastnase 1 Halter
	//translate([ds+bi/2-lr/2,ds,ds+hi-tr]) rotate ([-90,0,-90]) cylinder(h=tr, d=2*tr); // Rastnase 1 Rundung unten
	//translate([ds+bi/2+lr/2-tr/2,ds+tr/2,ds+hi]) sphere(, d=tr);			//Rastnase 1 Rundung oben
	//translate([ds+bi/2+lr/2-tr ,ds,ds+hi-tr])cube([tr,tr,tr]);			 //Rastnase 1 Halter
	//translate([ds+bi/2+lr/2-tr ,ds,ds+hi-tr])rotate ([-90,0,-90])cylinder(h=tr, d=2*tr); // Rastnase 1 Rundung unten
	//translate([ds+bi/2-lr/2+tr/2,ds+li-tr/2,ds+hi])sphere(, d=tr);                               //Rastnase 2 Rundung oben
	//translate([ds+bi/2-lr/2,ds+li-tr ,ds+hi-tr])cube([tr,tr,tr]);                                   //Rastnase 2 Halter
	//translate([ds+bi/2-lr/2,ds+li,ds+hi-tr])rotate ([-90,0,-90])cylinder(h=tr, d=2*tr);             // Rastnase 2 Rundung unten
	//translate([ds+bi/2+lr/2-tr/2,ds+li-tr/2,ds+hi])sphere(, d=tr);                                //Rastnase 2 Rundung oben
	//translate([ds+bi/2+lr/2-tr ,ds+li-tr ,ds+hi-tr])cube([tr,tr,tr]);                                   //Rastnase 2 Halter
	//translate([ds+bi/2+lr/2-tr ,ds+li   ,ds+hi-tr]) rotate ([-90,0,-90])cylinder(h=tr, d=2*tr);     // Rastnase 2 Rundung unten
 }

module deckel(){										// Deckel
	difference(){
		union(){
			translate([ds+fl,0,0]) cube([bi-2*fl,ds+li+ds,dd-kr ]);			// Deckel
			difference(){								// mit Abrundung an Kante
				hull(){
					translate([ds+fl,0+kr,dd-kr]) rotate([0,90,0]) cylinder(h=bi-2*fl, r=kr);
					translate([ds+fl,ds+li+ds-kr,dd-kr]) rotate([0,90,0]) cylinder(h=bi-2*fl, r=kr);}
				translate([ds-df+fl-20,0-20,-20]) cube([df+bi+df-2*fl+40,ds+li+ds+40,dd/2-2*fl+20]);}
			translate([ds-df+fl,0,0]) cube([df+bi+df-2*fl,ds+li+ds,dd/2-2*fl]);}// Deckelführung
			//translate([ds+bi/2-lr/2-fl,ds,0]) cube([lr+2*fl,tr,tr]);// Aussparung Rastnase
			//translate([ds+bi/2-lr/2-fl,ds+li-tr,0]) cube([lr+2*fl,tr,tr]);// Aussparung Rastnase
            }
		}

gehaeuse();
translate([0,ds+li+ds+3,0]) deckel();
