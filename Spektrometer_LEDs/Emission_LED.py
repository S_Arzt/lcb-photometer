import pandas as pd
import matplotlib.pyplot as plt

def formatting_data(input_file , slope_break = 0.2): # input_file

    raw_data = pd.read_excel(input_file  , skiprows = list(range(0,6)) , names = ["Wellenlaenge/nm","Werte"])

    data = raw_data.copy()
    #print(data)

    data_diff = data.diff(-1)
    data_slope = data["Werte"] / data["Wellenlaenge/nm"] 
    data = data[data_slope > slope_break].reset_index(drop = True)
    data_Werte_Max = data["Werte"].max()
    data["Werte"] = data["Werte"].apply(lambda x : (x/data_Werte_Max)*100)
    
    return data

rot = formatting_data("Excel_Daten/2019_03_18-rote_LED.xlsx")
blau = formatting_data("Excel_Daten/2019_03_18-blau_LED.xlsx" , 0.18)
gruen = formatting_data("Excel_Daten/2019_03_18-gruene_LED.xlsx")
gelb = formatting_data("Excel_Daten/2019_03_18-gelbe_LED.xlsx")
orange = formatting_data("Excel_Daten/2019_03_18-orange_LED.xlsx")
rgb_rot = formatting_data("Excel_Daten/2019_03_18-RGB_rote_LED.xlsx")
rgb_gruen = formatting_data("Excel_Daten/2019_03_18-RGB_gruene_LED.xlsx", 0.3)
rgb_blau = formatting_data("Excel_Daten/2019_03_18-RGB_blau_LED.xlsx",0.25)

list_Max = [["Farbe", "Wellenlänge Max"]]

fig , ax = plt.subplots(figsize=(7 , 5))
plt.rc('font', size=13)
plt.rc('legend', fontsize=10)

for LED , color , name , linetype in zip([rot,blau, gruen,gelb,orange,rgb_rot, rgb_gruen , rgb_blau]
                                         ,["#FF3300","#007AFF","#2BFF00","#FFD300","#FF7D00","#FF4200","#24FF00","#007AFF"]
                                         ,["rot","blau", "grün","gelb","orange","RGB_rot", "RGB_grün" , "RGB_blau"]
                                         ,["-","-","-","-","-","--","--","--"]):
    plt.plot(LED["Wellenlaenge/nm"] , LED["Werte"] , linestyle = linetype, label = name, color = color)
    list_Max.append([name , LED[LED["Werte"] == 100]["Wellenlaenge/nm"].values.tolist()[0]])

    

plt.xlabel("Wellenlänge [nm]")
plt.ylabel("Prozent")

ax.set_xlim(430 , 710)


plt.legend()

plt.savefig("LED_Spektren.pdf")
plt.show()


df_Max = pd.DataFrame(list_Max[1:] , columns = list_Max[0])
df_Max.to_csv("Max_Emission_LED.csv" , sep = ";" ,index = False)
